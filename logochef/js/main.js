function myScriptPostCheck(){
  var el = $('.neadtoshow.notshowed').first();
  if(!el.length){
    $(".more-works").show();
    $('#morelogo').remove();
  }
  else {
    el.addClass('coolpreviev');
  }
}

myScriptPostCheck();

function myScript(){
  var el = $('.neadtoshow.notshowed').first();
  if(el.length){
    el.addClass('showed');
    el.removeClass('notshowed');
    el.removeClass('coolpreviev');
  }
  myScriptPostCheck();
}

function loadStyleSheet(src) {
  "use strict";
  if (document.createStyleSheet) {
      document.createStyleSheet(src);
  } else {
      $("head").append($("<link rel='stylesheet' href='" + src + "' type='text/css' media='screen' />"));
  }
}

function loadScrypt(src) {
  "use strict";
  var script = document.createElement('script');
  script.src = src;
  script.type = 'text/javascript';
  script.async = true;
  script.charset = "UTF-8";
  document.head.appendChild(script);
}

function addCallbackkiller() {
  "use strict";
  loadStyleSheet("http://cdn.callbackkiller.com/widget/cbk.css");
  loadScrypt("http://cdn.callbackkiller.com/widget/cbk.js?wcb_code=d7ffb31490424c39112bc00e00909956");
}

var plan = 'Не вибрано';

$(document).ready(function () {
  "use strict";
  $('#exampleInputTel').mask("+38 (000) 000-00-00", {placeholder: "+38 (0__) ___-__-__"});
});
 
$(document).ready(function() {
  "use strict";
  loadStyleSheet("/largecss/main.css");
    
  var currentLang = 'en';
  if($('body.lang-ru').length){
      currentLang='ru';
      addCallbackkiller();
  }
  
  if($('body.lang-ua').length){
      currentLang='ua';
      addCallbackkiller();
  }
  
  $('.videoWrapper').on('click',function(e) {
      e.preventDefault();
      if(currentLang == 'en'){
          $('#myModal').modal('show')
      }
  });
  
  
  (function (d, w, c) {
      (w[c] = w[c] || []).push(function() {
          try {
              w.yaCounter36921510 = new Ya.Metrika({
                  id:36921510,
                  clickmap:true,
                  trackLinks:true,
                  accurateTrackBounce:true,
                  webvisor:true,
                  trackHash:true
              });
          } catch(e) { }
      });

      var n = d.getElementsByTagName("script")[0],
          s = d.createElement("script"),
          f = function () { n.parentNode.insertBefore(s, n); };
      s.type = "text/javascript";
      s.async = true;
      s.src = "http://mc.yandex.ru/metrika/watch.js";

      if (w.opera == "[object Opera]") {
          d.addEventListener("DOMContentLoaded", f, false);
      } else { f(); }
  })(document, window, "yandex_metrika_callbacks"); 
   
  
  $(function() {
    
    $("#moreworks").submit(function(event){
            "use strict";
            event.preventDefault();
            var subject = $("#moreworks-subject").val() || "no subject";
            var email = $("#moreworks-email").val() || "no email";
            $.ajax({
                type: "POST",
                url: "moreworks.php",
                data: "subject=" + subject + "&email=" + email + "&lang=" + currentLang + "&utm=" + location.search.replace(/&/g," ") || 'noUml',
                success : function(){
                    window.location= '/senk/'+currentLang+'.html';
                }
            });
      
    });
    
    $("#tenpersent").submit(function(event){
            "use strict";
            event.preventDefault();
            var name = $("#tenpersent-input").val() || "no name";
          
            $.ajax({
                type: "POST",
                url: "tenpersent.php",
                data: "name=" + name + "&lang=" + currentLang + "&utm=" + location.search.replace(/&/g," ") || 'noUml',
                success : function(){
                    window.location= '/senk/'+currentLang+'.html';
                }
            });
        });
    
    
    $("#subscriber").submit(function(event){
            "use strict";
            event.preventDefault();
            var name = $("#subscribe-name").val() || "no name";
            var email = $("#subscribe-email").val() || "no email";
          
            $.ajax({
                type: "POST",
                url: "subscribe.php",
                data: "name=" + name + "&email=" + email + "&lang=" + currentLang + "&utm=" + location.search.replace(/&/g," ") || 'noUml',
                success : function(resp){
                    window.location= '/senk/'+currentLang+'.html';
                }
            });
        });
    
    $("#modalForm").submit(function(event){
            "use strict";
            event.preventDefault();
            var name = $("#exampleInputName").val();
            var email = $("#exampleInputEmail").val();
            var tel = $("#exampleInputTel").val();
            if(currentLang == 'en'){
                tel="FROM_ENGLISH_123456";
            }

            if(tel.length != 19){
                var errMsg = 'Введите телефон в формате +38 (012) 345-67-89';
                if(currentLang == 'ua'){
                    errMsg = 'Введіть телефон в форматі +38 (012) 345-67-89';
                }
                $(".for-mob-tel>.form-group").addClass("has-error");
                $(".for-mob-tel>.form-group label").text(errMsg)
            }
            else{
                $(".for-mob-tel>.form-group").removeClass("has-error");
                $(".for-mob-tel>.form-group label").text($(".for-mob-tel>.form-group label").attr("data-label-origin"))
                $.ajax({
                    type: "POST",
                    url: "sendmail.php",
                    data: "lang=" + currentLang + "&name=" + name + "&email=" + email + "&tel=" + tel + "&utm=" + location.search.replace(/&/g," ") || 'noUml',
                    success : function(){
                        window.location= '/senk/'+currentLang+'.html';
                    }
                });
            }
        });
  });
  
  $('.change-link').on('click', function () {
      "use strict";
      location.hash = '#' + $( this ).data('newLink');
  });
    
  $('.change-plan').on('click', function () {
      "use strict";
      plan = $( this ).data('plan');
  });
  
  $('.fake-click').on('click', function () {
      "use strict";
    
      ga('send', 'event', 'own', 'tryCall');
      try { yaCounter36921510.reachGoal('tryCall') } catch(e) { }
      try { var href = $( this ).data('hhref'); } catch(e) { }
      $('<a>',{
          text: 'text',
          href: href,
          id: 'tmp-link'
      }).appendTo('body');
      $('#tmp-link')[0].click();
      $('#tmp-link')[0].remove();
  });
  
   
    
  WebFont.load({
    google: {
      families: ['Roboto:300,500,700','Bad Script&subset=cyrillic']
    }
  });
  
}); 

function gup( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}

$(document).ready(function(){
  console.info('i am alive');
  var source = gup( 'utm_source', location.href );
  var maybeDiscont = gup( 'utm_campaign', location.href );
  if( source === 'mytarget' && parseInt(maybeDiscont) === 7714109 ){
    $(".maydisc").addClass("active");
    $(".maydiscaccent").text("-11%");
    console.info('maybeDiscont',maybeDiscont);  
  }
  
  $("#closemaydisc").click(function(){
    $(".maydisc").addClass("minimalmode");
  })
  
  $(".maydiscshow").click(function(){
    $(".maydisc").removeClass("minimalmode");
  })
  
//  $("#getdisc").click(function(){
//    console.info($(".1price").text());
//  });
  
});

$(document).ready(function(){

  $('#heroimage').imagesLoaded(function() {
      $(".new-hero").addClass("loaded");
      $("#heroimage").addClass("loaded");
  });

});

function bootstrapBook(){
  var $mybook = $('#mybook');
  var $bttn_next= $('#next_page_button');
  var $bttn_prev= $('#prev_page_button');
  var $loading= $('#loading');
  var $mybook_images= $mybook.find('img');
  var cnt_images= $mybook_images.length;
  var loaded= 0;
  //preload all the images in the book,
  //and then call the booklet plugin

  $mybook_images.each(function(){
var $img = $(this);
var source= $img.attr('src');
$('<img/>').load(function(){
  ++loaded;
  if(loaded == cnt_images){
    $loading.hide();
    $bttn_next.show();
    $bttn_prev.show();
    $mybook.show().booklet({
    name:               null,                            // name of the booklet to display in the document title bar
    width:              710,                             // container width
    height:             473,                             // container height
    speed:              600,                             // speed of the transition between pages
    direction:          'LTR',                           // direction of the overall content organization, default LTR, left to right, can be RTL for languages which read right to left
    startingPage:       0,                               // index of the first page to be displayed
    easing:             'easeInOutQuad',                 // easing method for complete transition
    easeIn:             'easeInQuad',                    // easing method for first half of transition
    easeOut:            'easeOutQuad',                   // easing method for second half of transition

    closed:             false,                           // start with the book "closed", will add empty pages to beginning and end of book
    closedFrontTitle:   null,                            // used with "closed", "menu" and "pageSelector", determines title of blank starting page
    closedFrontChapter: null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank starting page
    closedBackTitle:    null,                            // used with "closed", "menu" and "pageSelector", determines chapter name of blank ending page
    closedBackChapter:  null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank ending page
    covers:             false,                           // used with  "closed", makes first and last pages into covers, without page numbers (if enabled)

    pagePadding:        10,                              // padding for each page wrapper
    pageNumbers:        true,                            // display page numbers on each page

    hovers:             false,                            // enables preview pageturn hover animation, shows a small preview of previous or next page on hover
    overlays:           false,                            // enables navigation using a page sized overlay, when enabled links inside the content will not be clickable
    tabs:               false,                           // adds tabs along the top of the pages
    tabWidth:           60,                              // set the width of the tabs
    tabHeight:          20,                              // set the height of the tabs
    arrows:             false,                           // adds arrows overlayed over the book edges
    cursor:             'pointer',                       // cursor css setting for side bar areas

    hash:               false,                           // enables navigation using a hash string, ex: #/page/1 for page 1, will affect all booklets with 'hash' enabled
    keyboard:           true,                            // enables navigation with arrow keys (left: previous, right: next)
    next:               $bttn_next,          // selector for element to use as click trigger for next page
    prev:               $bttn_prev,          // selector for element to use as click trigger for previous page

    menu:               null,                            // selector for element to use as the menu area, required for 'pageSelector'
    pageSelector:       false,                           // enables navigation with a dropdown menu of pages, requires 'menu'
    chapterSelector:    false,                           // enables navigation with a dropdown menu of chapters, determined by the "rel" attribute, requires 'menu'

    shadows:            true,                            // display shadows on page animations
    shadowTopFwdWidth:  166,                             // shadow width for top forward anim
    shadowTopBackWidth: 166,                             // shadow width for top back anim
    shadowBtmWidth:     50,                              // shadow width for bottom shadow

    before:             function(){},                    // callback invoked before each page turn animation
    after:              function(){}                     // callback invoked after each page turn animation
    });
    }
  }).attr('src',source);
  });
};

$(function() {
  if (window.innerWidth > 975){
    bootstrapBook();
    $('.readmore').readmore('destroy');

  }else{
    var moreLink = '<a href="#" class="moreLessReadMoreBtn">Read more</a>',
      lessLink = '<a href="#" class="moreLessReadMoreBtn">Close</a>';
    if ($('body.lang-ru').length) {
      moreLink = '<a href="#" class="moreLessReadMoreBtn">Больше</a>';
      lessLink = '<a href="#" class="moreLessReadMoreBtn">Закрыть</a>';
    }
    
    if ($('body.lang-ua').length) {
      moreLink = '<a href="#" class="moreLessReadMoreBtn">Докладніше</a>';
      lessLink = '<a href="#" class="moreLessReadMoreBtn">Закрити</a>';
    }
    
    $('.readmore').readmore({
      moreLink: moreLink,
      lessLink: lessLink
    });
  }
});