$(".nav li").on('mouseenter', function() {
var someElement = $(this),
    elm = $(this).find('ul').first();
elm.addClass('opening');
var timeoutId = setTimeout(function() {
    elm.addClass('opened');
}, 600);
someElement.data('timeoutId', timeoutId);
calculate_edge(this);
}).on('mouseleave', function() {
clearTimeout($(this).data('timeoutId'));
var elm = $(this).find('ul').first();
elm.removeClass('opened');
elm.removeClass('opening');
calculate_edge(this);
});
// jQuery

jQuery(".nav li a").on('click',function(event){
  event.preventDefault();
  var someElement = jQuery(this).parent(),
  elm = someElement.find('ul').first();
  if(elm.hasClass('opened')){ 
    clearTimeout(jQuery(this).parent().data('timeoutId'));
    var elm = jQuery(this).parent().find('ul').first();
    elm.removeClass('opened');
    elm.removeClass('opening');
  }
  else{
    elm.addClass('opening');
    var timeoutId = setTimeout(function() {
      elm.addClass('opened');
    }, 600);
    someElement.data('timeoutId', timeoutId);
  }
})