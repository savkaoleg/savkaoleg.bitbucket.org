//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var anchor = $(this);
        if(location.pathname == '/' || location.pathname == '/ua' || location.pathname == '/ua/' || location.pathname == '/en' || location.pathname == '/en/' ){
          
          console.info('qwe',$(anchor.attr('href').substring(1)).offset().top);
          
          $('html, body').stop().animate({
              scrollTop: $(anchor.attr('href').substring(1)).offset().top - 75
          }, 1500, 'easeInOutExpo');
          $(anchor.attr('href').substring(1)).blur();
          event.preventDefault();
        }
        else {
          window.location = anchor.attr('href');
        }
    });
});
