// loads various gulp modules
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    minifyCss = require('gulp-minify-css'),
    minifyJs = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    htmlmin = require('gulp-html-minifier'),
    uncss = require('gulp-uncss'),
    cssmin = require('gulp-cssmin'),
    imagemin = require('gulp-imagemin'),
    international = require('gulp-international'),
    clean = require('gulp-clean'),
    watch = require('gulp-watch'),
    connect = require('gulp-connect');

gulp.task('minifyhtml',['clean','copyimages', 'usemin'], function() {
  return gulp.src('./lib/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(international({
      whitelist: ['ru','en','ua'],
      verbose: true,
      filename:'${path}/${lang}.${ext}',
      encodeEntities:false
    }))
    .pipe(gulp.dest('./lib'))
});

gulp.task('saveLargeCss',['clean','usemin'], function () {
    return gulp.src("./lib/css/main.css")
      .pipe(gulp.dest("./lib/largecss"));
})

gulp.task('uncss',['clean','minifyhtml','usemin','saveLargeCss'], function () {
    return gulp.src('./lib/css/main.css')
    .pipe(uncss({
        html: ['./lib/ru.html'],
        ignore: ['.gradient']
    }))
		.pipe(cssmin())
    .pipe(gulp.dest('./lib/css'));
});

gulp.task('renameToIndex',['uncss','clean'], function () {
   return gulp.src("./lib/ru.html")
          .pipe(rename("index.html"))
          .pipe(gulp.dest("./lib"));
})

gulp.task('copyimages',['clean'], function() {
   return gulp.src('./images/**')
    //.pipe(imagemin())
    .pipe(gulp.dest('./lib/images'));
});

gulp.task('usemin',['clean'], function() {
    return gulp.src('index.html')
        .pipe(htmlmin())
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            css: [minifyCss({keepSpecialComments: 0}), 'concat'],
        }))
        .pipe(gulp.dest('./lib'));
});

gulp.task('clean', function () {
    return gulp.src('./lib', {read: false})
        .pipe(clean());
});
//
//gulp.task('watch', function() {
//    gulp.watch([paths.images], ['custom-images']);
//    gulp.watch([paths.styles], ['custom-css']);
//    gulp.watch([paths.scripts], ['custom-js']);
//    gulp.watch([paths.templates], ['custom-templates']);
//    gulp.watch([paths.index], ['usemin']);
//});

gulp.task('default', ['clean','copyimages', 'usemin', 'minifyhtml', 'saveLargeCss', 'uncss', 'renameToIndex']);

gulp.task('connect', function() {
  connect.server({
    root: 'lib',
    livereload: true
  });
});

gulp.task('dev',['default','connect']);