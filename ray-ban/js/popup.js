(function($){
	var _order = '';
  var methods = {
  
    init : function( options ) { 
	var _selector = this.selector;
		var $this = this;
		var settings = $.extend( {
		  'width'  : 400,
		  'height' : 200,
		  'top' : '8%',
		  'bgcolor' : '000000',
		  'opacity' : '0.7'
		}, options);
		
		if (!$('div').is('#popup-overflow'))
			$('body').append('<div id="popup-overflow" style="display:none;position:fixed;top:0;left:0;z-index:999;width:100%;height:100%;background:#'+settings.bgcolor+';opacity:'+settings.opacity+'"></div>');
		this.css('position',"fixed").css('width',settings.width+"px").css('height',settings.height+"px").css('display','none').css('left','50%').css('top',settings.top).css('z-index','1000').css('margin-left','-'+(settings.width)/2+'px');
		this.addClass('modal');
		$('#popup-overflow').click(function(){
			$this.popup('close');
			document.getElementById("blitz").style.display="none";
		});
		$('.modal-close').click(function(){
			$this.popup('close');
			document.getElementById("blitz").style.display="none";
		});
	  
    },
    open : function( ) {
	  $('.modal').popup('close');
	  $this = this;
	  $$ = $;
	  if (this.is('#order') ) {
		$frame = this.find('iframe');
		var iframe = $frame[0];
		iframe.src = iframe.src;
		$.fancybox.showLoading();
		iframe.onload = function(){
			$('#popup-overflow').show();
		    $this.fadeIn();
			$$.fancybox.hideLoading()
		}
	  }
	  else {
	  $('#popup-overflow').show();
	  this.fadeIn();
	  }
    },
    close : function( ) {
      $('#popup-overflow').hide();
	  $(this).fadeOut();
    },
    update : function( content ) {
      // !!!
    }
  };

  $.fn.popup = function( method ) {
    
    // логика вызова метода
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Метод с именем ' +  method + ' не существует для jQuery.popup' );
    } 
  };

})( jQuery );