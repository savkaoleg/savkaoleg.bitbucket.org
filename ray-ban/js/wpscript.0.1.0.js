function log(data){
	console.log(data)
}

(function( $ ){
  var methods = {
    init : function( options ) { 
		var settings = $.extend( {
			  'callbackButton' : 'callback-button',
			  'orderButton' : 'orderbutton',
			  popup : {}
			}, options);
		
		$('#'+settings.callbackButton).popup({height:250,width:350,top:'8%'});
		$('.'+settings.callbackButton).click(function(e){
			e.preventDefault();
			$('#'+settings.callbackButton).popup('open');
		});
		methods.callbackSbm($('#'+settings.callbackButton+' form'));
		$('#'+settings.orderButton).popup(settings.popup);
		$('.'+settings.orderButton).click(function(e){
			e.preventDefault();
			$('#'+settings.orderButton).popup('open');
		});
		$(document).ajaxStart(function(){
		$.fancybox.showLoading();
		});
		$(document).ajaxStop(function(){
			$.fancybox.hideLoading();
		});
    },
    callbackSbm : function($form) {
		  $form.submit(function (e){
            location.hash = '#thanks';
			e.preventDefault();
			$.ajax({
				url : 'sendemail.php',
				type:'POST',
				cache: false,
				dataType : 'json',
				data : $form.serialize(),

			}).done(function( data ) {
			console.log(data);
				if(data.success) {
					$form.find('input[type="text"]').val('');
					$form.find('input[type="submit"]').after('<div class="clb-msg" style="text-align:center;color:green;font-size:12px;">Спасибо! В ближайшее время мы Вам перезвоним!</div>');
					setTimeout(function(){$('#callback').popup('close');$('.clb-msg').remove();},3000);
				}
				else
					alert('Ошибка запроса обратного звонка');
		  });
	  })
    },
    slider : function(options ) {
	var settings = $.extend( {
			  'in' : 500,
			  'out' : 1000,
			  'previewbox' : 'preview',
			  'imagebox' : 'img-box',
			}, options);

		$('.'+settings.previewbox+' img').click(function(){
			_src = $(this).attr('src');
			$('.'+settings.imagebox+' img').fadeOut(settings.out,function(){
				$(this).attr('src',_src).fadeIn(settings.in);
			})
		})
    }
  };

  $.fn.wpScript = function( method ) {
	
    // логика вызова метода
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Метод с именем ' +  method + ' не существует для jQuery.tooltip' );
    } 
  };

})( jQuery );
