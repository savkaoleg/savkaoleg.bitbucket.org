import React, { Component, PropTypes } from 'react'

class Todo extends Component {
  render() {
    const { addTodo,deleteTodo } = this.props;
    return (
		<div className="form-group">
			<h1>To-Do <small>List</small></h1>
			<ul className="list-unstyled" id="todo">
			{this.props.todo.map(todo => 
				<li key={todo.id}>{todo.text}
					<button onClick={(function() {deleteTodo(todo.id);})}>
						<span className="glyphicon glyphicon-remove"></span>
					</button>
					<small>{todo.date}</small>
				</li>
			)}
			</ul>
			<form onSubmit={(e) => {e.preventDefault();if(this.refs.mess.value){addTodo(this.refs.mess.value);this.refs.mess.value = '';}}} role="form">
				<input className="form-control" ref='mess'/>
				<button className="btn btn btn-primary">Add</button>
			</form>
		</div>
    )
  }
}

Todo.propTypes = {
  addTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired
}

export default Todo
