$(document).ready(function() {
  $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      navigationText:["<img src='images/arrow_l.png'>","<img src='images/arrow_r.png'>"],
      autoPlay : 5000
  });
  $("#owl-demo_1").owlCarousel({
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      navigationText:["<img src='images/arrow_l.png'>","<img src='images/arrow_r.png'>"]
 
  });
  $("#owl-demo_2").owlCarousel({
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      navigationText:["<img src='images/arrow_l.png'>","<img src='images/arrow_r.png'>"]

 
  });
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: (target.offset().top-100)
        }, 1000);
        return false;
      }
    }
  });
  });
});
