<?php include 'header.php'; ?>
<section class="regulations">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2">Regulations</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Donec in ex dictum ante sagittis iaculis id in quam. Maecenas ac aliquam lorem, sed consectetur diam. Cras blandit vulputate justo, sit amet posuere ex fermentum in. Nam rutrum libero malesuada diam pretium, vitae pharetra magna laoreet. Sed lobortis convallis eros ac tempus. Vivamus efficitur nisl eget sem accumsan, ut laoreet libero molestie. Quisque imperdiet sapien et ante dapibus elementum pharetra a nisi. Ut tempus ut felis eget consequat. In vel viverra enim.</p>
                <p class="site-text">Morbi eu leo sed urna bibendum hendrerit. Cras ut cursus dolor. Fusce feugiat, arcu a lacinia luctus, elit tortor placerat tellus, at pulvinar dui sapien ac ligula. Nam dolor massa, suscipit id lorem eget, rhoncus malesuada ex. Praesent mi lorem, rutrum a lobortis iaculis, finibus id felis. Phasellus dapibus nunc eget ullamcorper semper. Curabitur pellentesque vitae neque vitae ultricies. Morbi eget diam ac:</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-very-small">Конституция Республики <br>Таджикистана</h2>
            </div>
            <div class="col-md-8">            
                <div class="file-item">
                    <img src="img/doc.png" alt="" class="img-responsive">
                    <a href="#" class="special-email">Конституция Республики <br>Таджикистана</a>
                    <p class="file-size">.PDF, 53 kb</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-very-small">Международно-правовые<br>акты</h2>
            </div>
            <div class="col-md-8">
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Всеобщая декларация прав человека</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Международный пакт 
об экономических, социальных и культурных правах</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Международный пакт о гражданских и политических правах</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Международная конвенция о защите прав всех трудящихся-мигрантов и членов их семей</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Шанхайская организация сотрудничества</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Евразийское экономическое сообщество</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Содружество Независимых Государств</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-very-small">Конституционные <br>Законы Республики <br>Таджикистана</h2>
            </div>
            <div class="col-md-8">
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О гражданстве РТ"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О судах РТ"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О Конституционном суде РТ"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О порядке решения вопросов административно-территориального устройства РТ"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О Горно-Бадахшанской автономной области"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "Об органах прокуратуры РТ"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О правовом статусе члена Маджлиси милли и депутата Маджлиси намояндагон Маджлиси Оли РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О местных органах государственной власти"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О Правительстве РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О Маджлиси Оли РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный 
закон РТ "О выборах депутатов в местные Маджлисы народных депутатов"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О выборах Маджлиси Оли РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О референдуме РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О правовом режиме чрезвычайного положения"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Конституционный закон РТ "О выборах Президента РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-very-small">Кодексы Республики Таджикистана</h2>
            </div>
            <div class="col-md-8">
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Налоговый кодекс РТ</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Кодекс РТ об административных правонарушениях
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Гражданский кодекс РТ (часть I)
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Гражданский кодекс РТ (часть II)
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Гражданский кодекс РТ (часть III)
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Гражданский процессуальный кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Экономический процессуальный кодекс РТ
Таможенный кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Таможенный кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Трудовой кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Жилищный кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Земельный кодекс РТ
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-very-small">Законы Республики <br>Таджикистана</h2>
            </div>
            <div class="col-md-8">
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О бухгалтерском учете и финансовой отчетности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об аудиторской деятельности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об оценочной деятельности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О Государственном бюджете РТ на 2016 год"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О страховании"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О страховой деятельности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О страховании сбережений физических лиц" 
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О Национальном банке Таджикистана"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О банковской деятельности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об исламской банковской деятельности"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О микрофинансовых организациях"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О несостоятельности (банкротстве)"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О финансовой аренде (лизинге)"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О рынке ценных бумаг"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об акционерных обществах"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об обществах с ограниченной ответственностью"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об инвестициях"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об иностранных инвестициях в РТ"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "Об инвестиционном соглашении"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
                   <div class="col-md-3">        
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О кредитных историях"
</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="col-md-3">           
                        <div class="file-item">
                            <img src="img/doc.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">Закон РТ "О борьбе с коррупцией"</a>
                            <p class="file-size">.PDF, 53 kb</p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        
    </div>
</section>
<?php include 'footer.php'; ?>