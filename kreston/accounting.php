<?php include 'header.php'; ?>
<section class="accounting">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2">Accounting <br>Services</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Kreston AC is able to provide a full range of accounting and outsourcing services to assist with the development and growth of your business both locally and internationally. The trusted relationship between member firms enables us to provide a coordinated response to your needs helping you to overcome barriers to international growth. <br></p>
                <p class="site-text" style="margin-top: 21px;">Accounting services include:</p>
                <ul class="site-text square">
                    <li>Preparation of financial statements in compliance with International Financial Reporting Standards (IFRS) or National Accounting Standards</li>
                    <li>Budgeting and business planning</li>
                    <li>Management accounting</li>
                    <li>Outsourced book-keeping and payroll</li>
                    <li>Outsourced company secretarial services including company formation;</li>
                    <li>Development of methodology and procedures of accounting</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>