$( document ).ready(function() {
    if($(document).width()>429){
        var width = 0;
        $('nav .dropdown-menu').each(function() {
            var item_width = $(this).width();
            if(item_width>width){
                width = item_width;
            }
        }).promise().done(function () { 
            $('nav .dropdown-menu').each(function() {
                $(this).css('width',width)
            })
        });  
        
        $(function() {
            $('.match-height').matchHeight();
        });
        
        $(function() {
            $('#adwantages .item').matchHeight();
        });
        
    }
    else{
        $( "#bs-example-navbar-collapse-1>ul" ).prepend( "<li id='lang-selector'></li>" );
        $('#lang ul').children().each(function(){
            var html = $(this).html();
//            $("<li>li</li>").appendTo('#bs-example-navbar-collapse-1 ul')
            $( "#bs-example-navbar-collapse-1 ul li#lang-selector" ).prepend( html );
        })
    }
}); 
//    $('.dropdown-menu>li>a').each(function(i,elem) {
//        var dateInner = $(this);
//        var wraps = [];
//        var count = 0;
//        $.each(dateInner.text().split(' '), function (key, value) {
//          var ind = (i+'-'+count);
//          count++;
//          wraps.push('<span class="f-b-t" data-id='+ind+'>' + value + '</span>');//<span class="f-b-b for-id-'+ind+'"/>
//        });
//        $(this).html(wraps.join(' '));
//    });
//    
//    
//    $('.dropdown-menu>li>a').hover(function() {
//        $( this ).find( ".line" ).each(function() {
//            $(this).remove();
//        })
//        var forAnimate;
//        var width;
//        var a = $( this );
//        var i = 1;
//        var lines = [];
//        var linesToDom = [];
//      $( this ).find( "span.f-b-t" ).each(function() {
//          width = $(this).width();
//          var offset = ($(this).position())
////          forAnimate = $(this).parent('a').find( "span.f-b-b.for-id-"+$(this).attr('data-id')+"" );
////          forAnimate.css({top:(offset.top+3),left:offset.left}).animate({
////            width:width+4,
////          }, 500 )
//          
//          lines.push({
//              top:(offset.top+3),
//              left:offset.left,
//              width:width+4
//          })
////          a.append('<span class="line line-'+i+'" style="top:'+(offset.top+3)+'px;left:'+offset.left+'px"></span>')
//          
//      }).promise().done(function () {
//          var end = lines.length-1;
//          $.each(lines, function (key, item) {
//            if(linesToDom.length){
//                var finded = false;
//                var linesEnd = linesToDom.length-1;
//                $.each(linesToDom, function (k, line) {
//                    if(line.top == item.top){
//                        // console.infoinfo('width',item.width,'top',item.top)
//                        line.width += item.width;
//                        // console.infotable('linesToDom before',linesToDom)
//                        linesToDom[k]=line;
//                        // console.infotable('linesToDom after',linesToDom)
//                        // console.infoinfo('new width',item.width,'top',item.top)
//                        // console.infotable(linesToDom)
//                    }
//                    else if (linesEnd == k){
//                        // console.infoinfo('push in each')
//                        // console.infotable('linesToDom before',linesToDom)
//                        linesToDom.push(item);
//                        // console.infotable('linesToDom after',linesToDom)
//                        // console.infotable(linesToDom)
//                    }
//                })
//            }
//            else {
//                // console.infoinfo('push first')
//                // console.infotable('linesToDom before',linesToDom)
//                linesToDom.push(item);   
//                // console.infotable('linesToDom after',linesToDom)
//                // console.infotable(linesToDom)       
//            }
//            if(key == end){
//                // console.infoinfo('final')
//                // console.infotable(linesToDom)
//                var linesEnd = linesToDom.length-1;
//                $.each(linesToDom, function (k, line) {
//                    a.append('<span class="line" data-w="'+line.width+'" style="top:'+line.top+'px;left:'+line.left+'px;"></span>')
//                    if (linesEnd == k){
//                        var an_count = 1;
//                        $( a ).find( ".line" ).each(function() {
//                            $(this).animate({
//                              width:$(this).attr('data-w')
//                            }, an_count*500 )
//                            an_count++;
//                        })
//                    }
//                })
//            }
//          })
//      });
//    }, function() {
//        $( this ).find( ".line" ).each(function() {
//            $(this).remove();
//        })
//    })
//});

$('textarea').change(function(){
    if($(this).val().length != 0) {
        $(this).addClass('has-val');
    }
    else{
        $(this).removeClass('has-val');
    }
})

;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'span' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));


$(document).ready(function() {
    
    $(document).click(function(){
        $.sidr('close', 'bs-example-navbar-collapse-1');
    })
    
    $('#right-menu').sidr({
      name: 'bs-example-navbar-collapse-1',
      side: 'right'
    });
    
    $("#sub-to-newsletter").submit(function(event){
        event.preventDefault();
        var email = $("#sub-to-newsletter-email").val();
        if(email){
            $.ajax({
                type: "POST",
                url: "sendmail.php",
                data: "email=" + email,
                success : function(){
                    var senkString = '<p class="thanks">Thank you, you have subscribed to our newsletter</p>'
                    $("#sub-to-newsletter").html(senkString)
                }
            });
        }
    });
    
    $("#contact-modal").submit(function(event){
        event.preventDefault();
        var name = $("#contact-modal-name").val();
        var email = $("#contact-modal-email").val();
        var phone = $("#contact-modal-phone").val();
        var textarea = $("#contact-modal-textarea").val();
        if(email){
            $.ajax({
                type: "POST",
                url: "sendmail.php",
                data: "name=" + name +"&email=" + email +"&phone=" + phone +"&textarea=" + textarea,
                success : function(){
                    var senkString = '<p class="thanks" style="height:'+$("#contact-modal").height()+'px;text-align:center;padding-top:'+($("#contact-modal").height()/2)+'px;">Thank you, your message sent! We will Contact you soon!</p>'
                    $("#contact-modal").html(senkString)
                }
            });
        }
    });
    
    
    $("#contact-us-form").submit(function(event){
        event.preventDefault();
        var name = $("#contact-us-form-name").val();
        var email = $("#contact-us-form-email").val();
        var phone = $("#contact-us-form-phone").val();
        var textarea = $("#contact-us-form-textarea").val();
        if(email){
            $.ajax({
                type: "POST",
                url: "sendmail.php",
                data: "name=" + name +"&email=" + email +"&phone=" + phone +"&textarea=" + textarea,
                success : function(){
                    var senkString = '<p class="thanks contact-us-thanks" style="height:'+$("#contact-us-form").height()+'px;padding-top:'+($("#contact-us-form").height()/2)+'px;">Thank you, your message sent! We will Contact you soon!</p>'
                    $("#contact-us-form").html(senkString)
                }
            });
        }
    });
    
    
    $("#career-form").submit(function(event){
        event.preventDefault();
        var name = $("#career-form-name").val();
        var email = $("#career-form-email").val();
        var phone = $("#career-form-phone").val();
        var textarea = $("career-form-textarea").val();
        if(email){
            $.ajax({
                type: "POST",
                url: "sendmail.php",
                data: "name=" + name +"&email=" + email +"&phone=" + phone +"&textarea=" + textarea,
                success : function(){
                    var senkString = '<p class="thanks contact-us-thanks" style="height:'+$("#career-form").height()+'px;padding-top:'+($("#career-form").height()/2)+'px;">Thank you, your message sent! We will Contact you soon!</p>'
                    $("#career-form").html(senkString)
                }
            });
        }
    });
});