<?php include 'header.php'; ?>
<section id="contuct_us">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2">contact us</h2>
            </div>
            <div class="col-md-8" >
                <p class="site-text" style="margin-top: -3px;">
                    Sed pharetra lorem eget orci lobortis, eu auctor felis porttitor. Phasellus nec consectetur lorem, vitae vestibulum risus. Integer et ligula rhoncus, condimentum lacus ut, pulvinar est. Cras justo eros, posuere nec aliquet id, pellentesque vitae ante. Sed mauris quam, lacinia at neque a, mattis maximus tellus. Vestibulum tristique diam vel elit efficitur suscipit. 
                </p>
                <div class="row site-text contacts-row" >
                    <div class="col-md-4 text-uppercase">
                        100 Rudaki Ave., <br>Dushanbe, tajikistan, 734001
                    </div>
                    <div class="col-md-4 text-center">
                        (+922 37) 883-23-22 <br> (+992) 90 777-23-22
                    </div>
                    <div class="col-md-4">
                        <a href="mailto:info@kpmt.tj" class="special-email">info@kpmt.tj</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contacts-form">
                            <form id="contact-us-form">
                                <input type="text" name="name" placeholder="YOUR NAME" id="contact-us-form-name">
                                <input type="text" name="email" placeholder="YOUR EMAIL ADDRESS" id="contact-us-form-email">
                                <input type="text" name="phone" placeholder="PHONE NO" id="contact-us-form-phone">
                                <textarea name="text" placeholder="MESSAGE" id="contact-us-form-textarea"></textarea>
                                <p class="text-left">
                                    <button type="submit" class="submit-ghost-btn-2">submit</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>