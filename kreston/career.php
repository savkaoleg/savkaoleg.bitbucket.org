<?php include 'header.php'; ?>
<section class="carrer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Why work for us?</h2>
            </div>
            <div class="col-md-8" >
                <p class="site-text">Kreston AC is built on deep foundations of trust, hard work and quality.</p>
                <p class="site-text">We implement these principles into everything we aim because we believe that together, we can make a difference. Our work makes an impact on our clients, and through that work we also have an impact in the marketplace and the public.</p>
                <p class="site-text">Our people embrace the responsibility they have to clients, our global network and each other. We have a clear sense of purpose when we come to work every day:  We believe that our work helps to build confidence in the market and strengthen the economy, ultimately driving progress and prosperity in the societies in which we live and work.</p>
                <p class="site-text">We want to work with people who share this sense of purpose. People who want to make a change for good and contribute positively to society. People who are driven to make a difference in the world in which they live.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Whom We Hire?</h2>
            </div>
            <div class="col-md-8" >
                <p class="site-text">It’s often said that it’s the people you work with that make a job special. At Kreston AC, we really believe that’s true. Because we value our staff, in order to open doors for them to value our clients. Our team of forward-thinking, professional experts creates long-term relationships and offers all the flexibility our clients need. By working in partnership, we help our clients build capital and sharpen their competitive edge, while of course fulfilling all their business, tax and wealth needs.</p>
                <p class="site-text">We are looking for hard worker, honest, creative and responsible staff, who will add value to everything they do at work and bring that value to our clients.</p>
                <p class="site-text">If this sounds like something you’d like to be a part of, please click through to our current vacancies. If it’s training you’re after, we also offer a range of courses to help set you on the path to success. We’ll work with you throughout your training, offering both professional and financial support along the way – so you enjoy a challenging and rewarding career with us.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Current <br>Opportunities </h2>
            </div>
            <div class="col-md-8" >
                <p class="site-text">Currently no open vacancy is available, but you can send your CV for consideration in our future vacancies. Please fill in form below.</p>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="contacts-form">
                            <form id="career-form">
                                <input type="text" name="name" placeholder="YOUR NAME" id="career-form-name">
                                <input type="text" name="email" placeholder="YOUR EMAIL ADDRESS" id="career-form-email">
                                <input type="text" name="phone" placeholder="PHONE NO" id="career-form-phone">
                                <div class="file-box">
                                    <input type="file" name="file-7[]" id="file-7" class="inputfile" data-multiple-caption="{count} files selected" multiple />
                                    <label for="file-7"><span></span> <strong> UPLOAD</strong></label>
                                </div>
                                <textarea name="text" placeholder="MESSAGE" id="career-form-textarea"></textarea>
                                <p class="text-left">
                                    <button type="submit" class="submit-ghost-btn-2">submit</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>                
</section>
<?php include 'footer.php'; ?>