    <a href="#" data-toggle="modal" data-target="#myModal">
        <section id="request-audit">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <div class="request-audit-btn">request your audit now</div>
                    </div>
                </div>
            </div>
        </section>
    </a>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <p class="rights">2016, <br>Kreston AC. <br>All rights reserved.</p>
                </div>
                <div class="col-md-8 col-sm-8">
                    <nav class="navbar navbar-default">
                      <div class="container-fluid">
                       <div class="row">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="navbar-collapse collapse in" id="bs-example-navbar-collapse-2">
                          <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Sector</a></li>
                            <li><a href="#">Press Centre</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                          </ul>
                        </div>
                        </div>
                      </div>
                    </nav>
                </div>
            </div>
            <div class="row row-padding">
                <div class="col-md-5 col-md-offset-4 col-sm-8 match-height">
                    <div class="subscribe">
                        <h4>Subscribe to our newsletter</h4>
                        <p>orbi lectus elit, ultricies non ornare eget, dapibus nec elit. Donec ornare dui dolor, vitae </p>
                        <form id="sub-to-newsletter">
                            <input type="text" placeholder="EMAIL ADDRESS" id="sub-to-newsletter-email">
                            <button type="submit">submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 col-sm-8 match-height">
                    <div class="social">
                        <div class="icons">
                            <div class="item item-print"></div>
                            <div class="item item-linkedin"></div>
                            <div class="item item-twitter"></div>
                            <div class="item item-facebook"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                   <a href="#">
                    <img src="img/made_in.png" class="made-in" alt="" class="img-responsive">
                   </a>
                </div>
                <div class="col-md-8 col-sm-8">
                    <p class="rights rights-show-small">2016, Kreston AC. All rights reserved.</p>
                    <p class="seo-text"> 
                        “KPMT Orien” LLC is local company providing high quality services in audit, advisory, valuation and outsourcing services. The Company has a license for providing audit services in RT No. 000024. 
Clientele of “KPMT Orien” LLC includes entities of much various industries, ownership modes and sizes – public agencies, industry enterprises, multiple small and medium size businesses of various industries and ownership modes. In addition, “KPMT Orien” performs audit engagements in cooperation with other major international audit/consulting firms, such as “Marka Audit Bishkek” – Member of HLB International, “Ansar Accountants” – Member of KRESTON International. Furthermore, “KPMT Orien” is also a member of Kreston International. Taking into consideration specifics the clients have to deal with, “KPMT Orien” since its very establishment has been aiming at complex servicing of its customers. Advancing along with the auditing market development trends and tending to meet standards of the world’s leading audit and consulting companies, we try to broaden the range of our services and increase their quality. “KPMT Orien” holds the License #0000056 for the right of performing audit, issued by the Ministry of Finance of the Republic of Tajikistan and the License #000024 for the right to perform valuation services, issued by the State Committee on Investment and State Property Management of the Republic of Tajikistan. The firm has Insurance Policy issued by JSC «BIMA» with insurance coverage amount of 50,000.00 somoni. The firm’s professional staff have certificate of the International Council of Certificated Accountants and Auditors, and hold a professional category “Certificated Accountant Practitioner” (CAP), CIPAEN. “KPMT Orien” LLC is local company providing high quality services in audit, advisory, valuation and outsourcing services. The Company has a license for providing audit services in RT No. 000024. Clientele of “KPMT Orien” LLC includes entities of much various industries, ownership modes and sizes – public agencies, industry enterprises, multiple small and medium size businesses of various industries and ownership modes. In addition, “KPMT Orien” performs audit engagements in cooperation with other major international audit/consulting firms, such as “Marka Audit Bishkek” – Member of HLB International, “Ansar Accountants” – Member of KRESTON International. Furthermore, “KPMT Orien” is also a member of Kreston International. Taking into consideration specifics the clients have to deal with, “KPMT Orien” since its very establishment has been aiming at complex servicing of its customers. Advancing along with the auditing market development trends and tending to meet standards of the world’s leading audit and consulting companies, we try to broaden the range of our services and increase their quality. “KPMT Orien” holds the License #0000056 for the right of performing audit, issued by the Ministry of Finance of the Republic of Tajikistan and the License #000024 for the right to perform valuation services, issued by the State Committee on Investment and State Property Management of the Republic of Tajikistan. The firm has Insurance Policy issued by JSC «BIMA» with insurance coverage amount of 50,000.00 somoni. The firm’s professional staff have certificate of the International Council of Certificated Accountants and Auditors, and hold a professional category “Certificated Accountant Practitioner” (CAP), CIPAEN.
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="img/x.png" alt="">
        </button>
           <div class="modal-wrapper">
                <p class="modal-title">
                    request your <br>audit now
                </p>
                <p>
                    Suspendisse potenti. Morbi tincidunt quam lectus, vitae posuere lectus lacinia at. Nunc elementum aliquam volutpat. Ut malesuada facilisis nulla a volutpat. Nam vitae lacinia metus, vitae congue nisl. Aliquam lacus nibh, ornare eu lobortis et
                </p>
                <form id="contact-modal">
                    <input type="text" name="name" placeholder="YOUR NAME" id="contact-modal-name">
                    <input type="text" name="email" placeholder="YOUR EMAIL ADDRESS" id="contact-modal-email">
                    <input type="text" name="phone" placeholder="PHONE NO" id="contact-modal-phone">
                    <textarea name="text" placeholder="MESSAGE" id="contact-modal-textarea"></textarea>
                    <p class="text-center">
                        <button type="submit" class="submit-ghost-btn">submit</button>
                    </p>
                </form>
           </div>
        </div>
      </div>
    </div>
	<script src="js/jquery.js"></script>
    <script src="js/jquery.matchHeight.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.sidr.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>