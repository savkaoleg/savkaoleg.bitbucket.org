<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>kreston</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
    <section id="lang">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-inline text-center">
                        <li>
                            <a href="#">RUS</a>
                        </li>
                        <li>
                            <a href="#" class="active">ENG</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>  
    </section>
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a id="right-menu" href="#right-menu" type="button" class="navbar-toggle collapsed" aria-expanded="false">
            <img src="img/menu.png" alt="">
          </a>
          <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="col-md-8 col-md-offset-4" style="margin-top: -105px;padding-right:0px;" >
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="#">Home</a></li>
            <li><a href="#">About us</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
              <ul class="dropdown-menu">
                <li class="spessial"></li>
                <li><a href="#">audit and assurance</a></li>
                <li><a href="#">accounting services</a></li>
                <li><a href="#">corporate and personal taxation</a></li>
                <li><a href="#">legal advising</a></li>
                <li><a href="#">appraisal and valuation</a></li>
                <li><a href="#">corporate finance</a></li>
                <li><a href="#">restructuring and insolvency</a></li>
                <li><a href="#">risk management</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sector</a>
              <ul class="dropdown-menu">
                <li class="spessial"></li>
                <li><a href="#">retail</a></li>
                <li><a href="#">manufacturing</a></li>
                <li><a href="#">technology and telecommunications, media and life science</a></li>
                <li><a href="#">travel, hospitality and leisure</a></li>
                <li><a href="#">professional services firms</a></li>
                <li><a href="#">real estate and construction</a></li>
                <li><a href="#">charity, not for profit and education</a></li>
                <li><a href="#">medical and healthcare</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Press Centre</a>
              <ul class="dropdown-menu">
                <li class="spessial"></li>
                <li><a href="#">publications</a></li>
                <li><a href="#">regulations</a></li>
              </ul>
            </li>
            <li><a href="#">Career</a></li>
            <li><a href="#">Contact us</a></li>
          </ul>
        </div>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <section id="poster"></section>
    </div>