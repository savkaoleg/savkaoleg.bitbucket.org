<?php include 'header.php'; ?>
<section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h2>about<br>kreston&nbsp;AC</h2>
                </div>
                <div class="col-md-8 col-sm-8">
                    <p>Kreston International Limited is a global network of independent accounting firms. Founded in 1971 we offer reliable and convenient access to quality services through member firms located around the globe. Currently ranking as the 12th largest accounting network in the world, Kreston now covers over 100 countries and provides a resource of over 21,000 professional and support staff.
                    </p><p>In today's economic climate, with rapidly developing new markets, improved communications, and greater international mobility, businesses operate on an increasingly global scale. Successful global organisations choose to use the services of accountants and advisors in each individual country, to take advantage of their local expertise and contacts.
                    </p><p>Whether you are entering the international business arena for the first time, or building upon an existing global enterprise, Kreston members are there to offer you extensive professional services. Kreston member firms are accustomed to working closely together on client assignments to deliver international solutions.
</p>
                <p class="text-right">
                    <a href="#" class="ghost-btn">read more</a>
                </p>
                </div>
            </div>
        </div>
    </section>
<section id="adwantages">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h2 class="text-left">Advantages <br>of working <br>with US</h2>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <img src="img/adwantages/1.png" alt="" class="img-responsive">
                                <h3>local knowledge and global experience</h3>
                                <p>donec eu pharetra nibh. Maecenas pellentesque metus vitae </p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <img src="img/adwantages/2.png" alt="" class="img-responsive">
                                <h3>personal servicing</h3>
                                <p>donec posuere efficitur arcu, quis mattis lacus consectetur vitae</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <img src="img/adwantages/3.png" alt="" class="img-responsive">
                                <h3>customer loyalty</h3>
                                <p>cras nulla libero, bibendum vitae consectetur sit amet, aliquet nec nisl</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <img src="img/adwantages/4.png" alt="" class="img-responsive">
                                <h3>quality</h3>
                                <p>etiam lacus ante, rutrum ut bibendum at, rhoncus in tortor. Cras tincidunt urna vel dolor dignissim tincidunt</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <img src="img/adwantages/5.png" alt="" class="img-responsive">
                                <h3>uniqueness and reliably</h3>
                                <p>etiam quis ullamcorper mauris, in dictum urna. Integer consectetur placerat convallis</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php'; ?>