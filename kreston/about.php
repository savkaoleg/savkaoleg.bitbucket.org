<?php include 'header.php'; ?>
<section class="about-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2">about us</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Kreston International Limited is a global network of independent accounting firms. Founded in 1971 we offer reliable and convenient access to quality services through member firms located around the globe. Currently ranking as the 12th largest accounting network in the world, Kreston now covers over 100 countries and provides a resource of over 21,000 professional and support staff.
    </p><p class="site-text">In today's economic climate, with rapidly developing new markets, improved communications, and greater international mobility, businesses operate on an increasingly global scale. Successful global organisations choose to use the services of accountants and advisors in each individual country, to take advantage of their local expertise and contacts.
    </p><p class="site-text">Whether you are entering the international business arena for the first time, or building upon an existing global enterprise, Kreston members are there to offer you extensive professional services. Kreston member firms are accustomed to working closely together on client assignments to deliver international solutions.
Our members, who are principals in their own firms, provide a personal and confidential service, while looking after your business interests. Kreston members strive to provide the highest quality expertise available in their respective country.
    </p><p class="site-text">The first-hand knowledge of local regulations and customs provided by your Kreston member firm can give your organisation an invaluable competitive advantage in the world market place. Working with an established firm in your destination country, you can take advantage of their contacts and local reputation. If international jurisdictions complicate the issue, individual Kreston members can call upon the support of the worldwide network to provide a swift, appropriate solution.
</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">One-to-One <br>Contact</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">By choosing a Kreston International member you have reduced the learning curve and the risk when entering new markets. You maintain control through one-to-one contact with the local Kreston member.
                </p><p class="site-text">Whatever your business, accounting and consulting requirements, a Kreston member will be there to provide expert support. Where appropriate, Kreston members find and work in close co-operation with other specialists. Each Kreston International member is an independent practice with sole responsibility for its own work, staff and clients. You are assured of continuity of service and personal attention at all times.
                </p><p class="site-text">The strength of the relationship between the independent firms within Kreston International adds a powerful extra dimension to this personal service by providing 24-hour global coverage and removing the need to negotiate cultural bridges.
</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Commitment to <br>Quality</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Kreston International member firms commit to compliance with the professional standards appropriate in their respective countries and to adhere to the following international standards:</p>
                <ul class="site-text square">
                    <li>International Standards on Quality Control</li>
                    <li>International Standards on Auditing for the conduct of transnational audits.</li>
                    <li>Code of Ethics as issued by the International Ethics Standards Board for Accountants.</li>
                </ul>
                <p class="site-text">A globally coordinated quality monitoring and review programme supports member firms in the maintenance of these standards. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Forum of Firms</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Kreston International is a member of the Forum of Firms. The forum is an association of international networks of accounting firms, its goal being to promote consistent and high-quality standards of financial reporting and auditing practices worldwide.</p>
                <p class="site-text">For additional details please go to <a href="www.forumoffirms.org" class="special-email">www.forumoffirms.org</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Legal Structure <br>and Disclaimer</h2>
            </div>
            <div class="col-md-8">
                <p class="site-text">Kreston International is a global network of accounting firms each of which is a separate and independent legal entity and as such has no liability for the acts or omissions of any other member firm. Kreston International Limited is a company registered in England (No:3453194) and limited by guarantee. Kreston International Limited provides no services to clients and has no liability for the acts or omissions of any member firm. Registered office: Springfield Lyons Business Centre, Springfield Lyons Approach, Chelmsford, Essex, CM2 5LB. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">Licenses <br>and accreditation</h2>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="file-item">
                            <img src="img/licenses.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">license for Audit Reg. No 000024</a>
                            <p class="file-size">.PDF, 53 kb</p>
                            <p class="site-text">LLC "KPMT ORIEN" has a license for Audit. Reg.No 000024, valid till May 16, 2019</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="file-item">
                            <img src="img/licenses.png" alt="" class="img-responsive">
                            <a href="#" class="special-email">license for Valuation Services <br>Reg.No 000024	</a>
                            <p class="file-size">.PDF, 216 kb</p>
                            <p class="site-text">LLC "KPMT ORIEN" has a license for Valuation Services. Reg.No 000024, valid till July 13, 2019</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2 class="site-h2 site-h2-small">our team</h2>
            </div>
            <div class="col-md-8">
                <div class="row team-row">
                    <div class="col-md-4">
                        <img src="img/team.png" alt="" class="img-responsive">
                        <h4>James SMITT</h4>
                        <h5>founder</h5>
                        <p class="site-text">Maecenas ut sodales ligula. Morbi ac congue est, id convallis nunc. Mauris sagittis ante sit amet sapien elementum, blandit consequat libero blandit</p>
                    </div>
                    <div class="col-md-4">
                        <img src="img/team-2.png" alt="" class="img-responsive">
                        <h4>JORDAN o’collahan</h4>
                        <h5>co-founder</h5>
                        <p class="site-text">Proin semper erat et quam cursus, gvravida efficitur est facilisis. Etiam mattis quis risus at bibendum. In ultricies augue tortor, quis mattis elit</p>
                    </div>
                    <div class="col-md-4">
                        <img src="img/team-3.png" alt="" class="img-responsive">
                        <h4>ketrin SPENCER</h4>
                        <h5>CEO</h5>
                        <p class="site-text">Curabitur sapien nisl, varius quis interdum vitae, tempor quis odio. Pellentesque pharetra tempus tincidunt. Etiam lacus dolor, volutpat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php'; ?>