import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import Apptodo from './containers/todoApp'
import configureStore from './store/configureStore'

const store = configureStore()

render(
  <Provider store={store}>
    <Apptodo />
  </Provider>,
  document.getElementById('root')
)
