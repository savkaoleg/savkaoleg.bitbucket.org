import {ADD_TODO,DELETE_TODO} from '../actions/todo'
const initialState = [
  {
    text: 'Use Redux',
    date: 'December 29th 2015, 11:11:35 am',
    id: 1451381854019
  }
]
export default function todo(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
       return state.concat({
          text: action.text,
          date: moment().format('MMMM Do YYYY, h:mm:ss a'),
          id: Date.now()
        })

    case DELETE_TODO:
       return state.filter(todo =>
          todo.id !== action.id
       )
    default:
      return state
  }
}
