var $grid = $('.grid').masonry({
  itemSelector: '.grid-item',
  percentPosition: true
});

$grid.imagesLoaded().progress( function() {
  $grid.masonry('layout');
});


var $container = $('.portfolio');
$container.imagesLoaded().progress( function() {
	$container.isotope({
		filter: '*',
		animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false,
		}
	});
	$('#portfolio .filters a').click(function(){
		var selector = $(this).attr('data-filter');
		$container.isotope({
			filter: selector,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false,
			}
		});
	  return false;
	});
	var $optionSets = $('#portfolio .filters'),
	       $optionLinks = $optionSets.find('a');
	       $optionLinks.click(function(){
	          var $this = $(this);
		  // don't proceed if already selected
		  if ( $this.hasClass('selected') ) {
		      return false;
		  }
	   var $optionSet = $this.parents('#portfolio .filters');
	   $optionSet.find('.selected').removeClass('selected');
	   $this.addClass('selected'); 
	});
});

var $revcontainer = $('.reviews');
$revcontainer.imagesLoaded().progress( function() {
	$revcontainer.isotope({
		filter: '.first',
		animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false,
		}
	});
	$('#reviews .filters a').click(function(){
		var selector = $(this).attr('data-filter');
		$revcontainer.isotope({
			filter: selector,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false,
			}
		});
	  return false;
	});
	var $optionSets = $('#reviews .filters'),
	       $optionLinks = $optionSets.find('a');
	       $optionLinks.click(function(){
	          var $this = $(this);
		  // don't proceed if already selected
		  if ( $this.hasClass('selected') ) {
		      return false;
		  }
	   var $optionSet = $this.parents('#reviews .filters');
	   $optionSet.find('.selected').removeClass('selected');
	   $this.addClass('selected'); 
	});
});

$(window).scroll(function() {
    if (document.body.scrollTop > window.innerHeight) {
        $(".navbar").addClass("top-nav-collapse");
        $(".navbar").addClass("navbar-fixed-top");
    } else {
        $(".navbar").removeClass("top-nav-collapse");
        $(".navbar").removeClass("navbar-fixed-top");
    }
});

$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});
